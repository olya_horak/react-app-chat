import React from 'react';
import moment from 'moment';

class Message extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            isLiked: false
        };
    }

    like = () => {
        this.setState({
            isLiked: true
        })
    }

    render() {
        const message = this.props.message || {};

        let messageTime = message.createdAt;
        const likedClassName = this.state.isLiked ? "message-liked" : "message-like"

        return (

            <div className="message">
                <div className="message-text">
                    {message.text}
                </div>
                <button className={likedClassName} onClick={this.like}>
                    <i className="fas fa-heart"></i>
                </button>

                <img className="message-user-avatar" src={message.avatar} alt="ava" />

                <span className="message-user-name">
                    {message.user}
                </span>
                at
                <span className="message-time">
                    {messageTime = moment(messageTime).format("HH:mm")}
                </span>


            </div>
        );
    }
}
export default Message;
