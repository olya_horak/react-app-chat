import React from 'react';
import Header from './Header';
import MessageList from './MessageList';
import MessageInput from './MessageInput';
import Preloader from './Preloader';




class Chat extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            messages: [],
            isLoading: false,
            myName: "Olya"
        };
    }

    componentDidMount() {
        const currentState = { ...this.state };
        currentState.isLoading = true;
        this.setState(currentState)

        fetch(this.props.url)
            .then((response) => {
                return response.json();

            })
            .then((messages) => {
                messages = messages.sort((m1, m2) => new Date(m1.createdAt) - new Date(m2.createdAt));

                console.log(messages);
                this.setState({
                    messages: messages,
                    isLoading: false
                });

            });
    }

    generateId() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r && 0x3 | 0x8);
            return v.toString(16);
        });
    }

    deleteMessage = (msg) => {
        const messages = this.state.messages;
        const index = messages.indexOf(msg);
        messages.splice(index, 1);
        this.setState({ messages: messages });
    }

    createMessage(inputValue) {
        const newMessage = {
            id: this.generateId(),
            userId: "533b5123-1b8f-22e8-9629-c7eca82aa7bd",
            avatar: "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
            user: this.state.myName,
            text: inputValue,
            createdAt: new Date(Date.now()),
            editedAt: ""
        }
        return newMessage;
    }

    addMessage = (inputValue) => {
        const myMessage = this.createMessage(inputValue);
        this.state.messages.push(myMessage);
        this.setState({
            messages: this.state.messages,
        })
    }



    render() {
        const messages = this.state.messages;

        return (
            <div className="Chat flex-col">
                <Header messages={messages} />
                <MessageList messages={messages} onDeleteMessage={this.deleteMessage} />
                <MessageInput onSentMessage={(text) => this.addMessage(text)} />
                <Preloader isLoading={this.state.isLoading} />
            </div>


        );
    }
}




export default Chat;