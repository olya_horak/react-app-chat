import './App.css';
import Chat from "./components/Chat";


const chatUrl = 'https://edikdolynskyi.github.io/react_sources/messages.json';

function App() {

  return (
    <div className="App">
      <Chat url={chatUrl}/>
    </div>
  );
}

export default App;
